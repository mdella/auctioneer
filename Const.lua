local Const = Auctioneer:Boot("Const")
local iota = 0

-- Call to get a unique ID for your own module's events.
function Const:Iota()
	iota = iota + 1
	return iota
end

-- Fired when the AH opens or closes.
Const.AuctionHouseOpened = Const:Iota()
Const.AuctionHouseClosed = Const:Iota()

-- Events from AH.
Const.ReplicateItemsAvaliable = Const:Iota()
Const.BrowseResultsAvailable = Const:Iota()
Const.SearchResultsAvaliable = Const:Iota()
Const.ItemKeyInfoFound = Const:Iota()
